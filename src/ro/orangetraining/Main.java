package ro.orangetraining;
import java.util.*;


public class Main {

    public static void main(String[] args) {
        // write your code here
        List<String> CurrentEmployees = new ArrayList<>();
        String[] MyHotelEmployees={"Will Smith", "Benjamin McCartney", "Albert Davidson","Jaden Clinton","Donald Lincoln","Bart Bureau", "Alistaire O'Doherty"};
        for (int i=0;i<MyHotelEmployees.length;i++){
            CurrentEmployees.add(MyHotelEmployees[i]);

        }
        System.out.println("The number of employees of the hotel is: "+ CurrentEmployees.size());
        System.out.println("Their names are: "+CurrentEmployees);
        System.out.println("The employee at first floor is: "+CurrentEmployees.get(1));
        System.out.println("The designated employee on the first floor was fired for innappropiate behaviour.");
        CurrentEmployees.remove(1);
        System.out.println("The new list of employees is: "+CurrentEmployees);
        System.out.println("The new number of employees of the hotel is: "+ CurrentEmployees.size());
        // folosirea operatorului :: pentru a invoca println fara argumente pentru a lista un element pe rand
        //este o metoda mai usoara decat folowirea unui if sau while...
        CurrentEmployees.forEach(System.out::println);
        //adaugam un nou angajat pentru etajul 1
        CurrentEmployees.add(1,"Steve Irwin");
        System.out.println("The new substitute will be "+CurrentEmployees.get(1));
        //lista finala
        System.out.println("The final list of employees is : "+CurrentEmployees);








    }
}
